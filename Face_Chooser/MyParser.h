//
//  MyParser.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 17.03.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyParser : NSObject <NSXMLParserDelegate>

@property NSMutableArray *arrayOfURL;
@property NSMutableArray *arrayOfNames;
@property NSXMLParser *theParser;

-(void)letsParseWithURL:(NSString *)url;

@end

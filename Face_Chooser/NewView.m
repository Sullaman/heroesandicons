//
//  NewView.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 13.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "NewView.h"

@interface NewView ()

@end

@implementation NewView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end

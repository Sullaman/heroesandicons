//
//  Person.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 10.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "Person.h"

@implementation Person

-(id)init{
    if (self = [super init]) {
        _name = @"Unnamed person";
        _image = nil;
    }
    return self;
}

@end

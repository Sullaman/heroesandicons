//
//  AppDelegate.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 10.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  MyPlayer.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 24.04.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "MyPlayer.h"

@implementation MyPlayer

- (void)playContentOfFile:(NSString *)name
{    
    NSString* path = [[NSBundle mainBundle] pathForResource:name
                                                     ofType:@"mp3"];
    NSURL* url = [NSURL fileURLWithPath:path];
    
    NSError *error;
    thePlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url
                                                       error:&error];
    
    [thePlayer setVolume:0.3];
    
    [thePlayer play];
}


- (void)playWhenAnswerIsCorrect
{
    
    files = [NSArray arrayWithObjects:@"bell1", @"bell2", @"bell3", nil];
    
    srandom((unsigned)time(NULL));
    NSInteger randex = (int)(random() % 3);
    
    [self playContentOfFile:[files objectAtIndex:randex]];
}

- (void)playWhenAnswerIsIncorrect
{
    [self playContentOfFile:@"bellWrong"];
}

- (void)playWhenExit
{
    [self playContentOfFile:@"bellExit"];
}

- (void)playWhenLaunch
{
    [self playContentOfFile:@"bellLaunch"];
}

- (void)playWhenTip
{
    [self playContentOfFile:@"bellTip"];
}

- (void)playWhenWin
{
    [self playContentOfFile:@"bellYouWin"];
}
- (void)playWhenLoad
{
    [self playContentOfFile:@"bellLoad"];
}

@end

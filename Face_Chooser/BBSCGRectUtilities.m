#import "BBSCGRectUtilities.h"

#define WIDTH(rect)       (rect.size.width)
#define HEIGHT(rect)      (rect.size.height)

#define MIN_X(rect)       (rect.origin.x)
#define MIN_Y(rect)       (rect.origin.y)

#define MAX_X(rect)       (rect.origin.x + WIDTH(rect))
#define MAX_Y(rect)       (rect.origin.y + HEIGHT(rect))

#define STANDARDIZE(rect) CGRectStandardize(rect)

// Rects must be standardized
static CGFloat centerX(CGRect rect, CGRect ofRect)
{
    return MIN_X(ofRect) + floorf((WIDTH(ofRect) - WIDTH(rect)) / 2);
}

// Rects must be standardized
static CGFloat centerY(CGRect rect, CGRect ofRect)
{
    return MIN_Y(ofRect) + floorf((HEIGHT(ofRect) - HEIGHT(rect)) / 2);
}

CGRect BBSCGRectWithSize(CGFloat width, CGFloat height)
{
    return CGRectMake(0, 0, width, height);
}

CGRect BBSCGRectWithOrigin(CGFloat x, CGFloat y)
{
    return CGRectMake(x, y, 0, 0);
}

CGRect BBSCGRectSetX(CGRect rect, CGFloat x)
{
    return CGRectMake(x, rect.origin.y, rect.size.width, rect.size.height);
}

CGRect BBSCGRectSetY(CGRect rect, CGFloat y)
{
    return CGRectMake(rect.origin.x, y, rect.size.width, rect.size.height);
}

CGRect BBSCGRectSetWidth(CGRect rect, CGFloat width)
{
    return CGRectMake(rect.origin.x, rect.origin.y, width, rect.size.height);
}

CGRect BBSCGRectSetHeight(CGRect rect, CGFloat height)
{
    return CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, height);
}

CGRect BBSCGRectSetXY(CGRect rect, CGFloat x, CGFloat y)
{
    return CGRectMake(x, y, rect.size.width, rect.size.height);
}

CGRect BBSCGRectSetWidthHeight(CGRect rect, CGFloat width, CGFloat height)
{
    return CGRectMake(rect.origin.x, rect.origin.y, width, height);
}

CGRect BBSCGRectSetSize(CGRect rect, CGSize size)
{
    return BBSCGRectSetWidthHeight(rect, size.width, size.height);
}

CGRect BBSCGRectCenterLeft(CGRect rect, CGRect ofRect, CGFloat padding)
{
    rect = STANDARDIZE(rect);
    ofRect = STANDARDIZE(ofRect);
    return CGRectMake(MIN_X(ofRect) - WIDTH(rect) - padding
                      , centerY(rect, ofRect)
                      , WIDTH(rect)
                      , HEIGHT(rect));
}

CGRect BBSCGRectCenterRight(CGRect rect, CGRect ofRect, CGFloat padding)
{
    rect = STANDARDIZE(rect);
    ofRect = STANDARDIZE(ofRect);
    return CGRectMake(MAX_X(ofRect) + padding
                      , centerY(rect, ofRect)
                      , WIDTH(rect)
                      , HEIGHT(rect));
}

CGRect BBSCGRectCenterBottom(CGRect rect, CGRect ofRect, CGFloat padding)
{
    rect = STANDARDIZE(rect);
    ofRect = STANDARDIZE(ofRect);
    return CGRectMake(centerX(rect, ofRect)
                      , MAX_Y(ofRect) + padding
                      , WIDTH(rect)
                      , HEIGHT(rect));
}

CGRect BBSCGRectCenterTop(CGRect rect, CGRect ofRect, CGFloat padding)
{
    rect = STANDARDIZE(rect);
    ofRect = STANDARDIZE(ofRect);
    return CGRectMake(centerX(rect, ofRect)
                      , MIN_Y(ofRect) - HEIGHT(rect) - padding
                      , WIDTH(rect)
                      , HEIGHT(rect));
}

CGSize BBSCGSizeAspectScaleToSize(CGSize fromSize, CGSize toSize)
{
    const CGFloat curAspectRatio = fromSize.width / fromSize.height;
    const CGFloat targetAspectRatio = toSize.width / toSize.height;
    if (curAspectRatio > targetAspectRatio) {
        return CGSizeMake(toSize.width, toSize.width / curAspectRatio);
    }
    return CGSizeMake(toSize.height * curAspectRatio, toSize.height);
}

//
//  APBaseViewController.m
//  HeroesAndIcons
//
//  Created by Алексей Поляков on 20.10.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "APBaseViewController.h"

@interface APBaseViewController ()

@end

@implementation APBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _iAdBanner = [[ADBannerView alloc] initWithFrame:CGRectZero];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    [_iAdBanner setFrame:CGRectOffset(_iAdBanner.frame, 0, screenRect.size.height - _iAdBanner.frame.size.height)];
    _iAdBanner.backgroundColor = [UIColor redColor];
    _iAdBanner.delegate = self;
    
    [self.view addSubview:_iAdBanner];
}

#pragma mark iAd

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{

    BOOL shouldExecuteAction = YES;
    return shouldExecuteAction;
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
}


@end

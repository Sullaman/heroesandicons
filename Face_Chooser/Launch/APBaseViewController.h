//
//  APBaseViewController.h
//  HeroesAndIcons
//
//  Created by Алексей Поляков on 20.10.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface APBaseViewController : UIViewController <ADBannerViewDelegate>

@property ADBannerView *iAdBanner;

@end

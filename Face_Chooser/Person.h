//
//  Person.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 10.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property UIImage *image;
@property NSString *name;

@end

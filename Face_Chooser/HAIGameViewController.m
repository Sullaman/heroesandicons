//
//  ViewController.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 10.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "HAIGameViewController.h"
#import "AMBlurView.h"

@interface HAIGameViewController (){
    
    MyPlayer *megaPlayer;
    CGFloat progress;
}

@end

@implementation HAIGameViewController

@synthesize category;

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    self.view.backgroundColor = [UIColor whiteColor];
    _scoreLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"score.png"]];
    
    arrayOfImagesURL = [[NSMutableArray alloc] init];
    arrayOfNames = [[NSMutableArray alloc] init];
    
    urlXML = [HAIMenuViewController categoryURL];
    
    megaPlayer = [[MyPlayer alloc]init];
    [megaPlayer playWhenLaunch];
    
    _spinnerView = [[STKSpinnerView alloc] initWithFrame:_coundDownLable.frame];
    [self.view addSubview:_spinnerView];
    [self.view bringSubviewToFront:_coundDownLable];
    
    [_spinnerView setImage:[UIImage imageNamed:@"clearСircle.png"]];
    [_spinnerView setAlpha:0.9];
    
    progress = 0;
    [self startGame];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height == 568.0f)
    {
        _buttonNextImage.frame      = CGRectOffset(_buttonNextImage.frame, 0, 15);
        _button50.frame             = CGRectOffset(_button50.frame, 0, 15);
        _buttonGiveMeMoreTime.frame = CGRectOffset(_buttonGiveMeMoreTime.frame, 0, 15);
    }
    
    [self.view addSubview:self.iAdBanner];
    [self.iAdBanner setDelegate:self];
    if (self.iAdBanner.isBannerLoaded)
    {
        self.iAdBanner.alpha = 1;
        _lineViewBottom.frame = BBSCGRectSetY(_lineViewBottom.frame, CGRectGetMinY(self.iAdBanner.frame) - 1);
         [self viewDidLayoutSubviews];
    }
    else
    {
        self.iAdBanner.alpha = 0;
        _lineViewBottom.frame = BBSCGRectSetY(_lineViewBottom.frame, CGRectGetMaxY(self.iAdBanner.frame) - 1);
         [self viewDidLayoutSubviews];
    }
    self.iAdBanner.backgroundColor = _lineView1.backgroundColor;
}


- (void)viewDidLayoutSubviews
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    if (screenRect.size.height == 568.0f)
    {
        [self replaceButttonsWithTopPoint:CGRectGetMaxY(_buttonNextImage.frame) + 10 bottomPoint:CGRectGetMinY(_lineViewBottom.frame) - 2];
    }
    else
    {
        [self replaceButttonsWithTopPoint:CGRectGetMaxY(_lineViewTop.frame) + 1 bottomPoint:CGRectGetMinY(_lineViewBottom.frame) - 2];
    }
}

- (void)replaceButttonsWithTopPoint:(CGFloat)topY bottomPoint:(CGFloat)bottomY
{
    CGFloat midY = (bottomY - topY) / 2;
    CGFloat buttonHeiht = (midY / 2) - 2;
    
    _button1.frame = BBSCGRectSetHeight(_button1.frame, buttonHeiht);
    _button2.frame = BBSCGRectSetHeight(_button2.frame, buttonHeiht);
    _button3.frame = BBSCGRectSetHeight(_button3.frame, buttonHeiht);
    _button4.frame = BBSCGRectSetHeight(_button4.frame, buttonHeiht);
    
    _lineView1.frame = BBSCGRectSetHeight(_lineView1.frame, 1);
    _lineView2.frame = BBSCGRectSetHeight(_lineView2.frame, 1);
    _lineView3.frame = BBSCGRectSetHeight(_lineView3.frame, 1);
    
    _button1.frame   = BBSCGRectSetY(_button1.frame, topY);
    _lineView1.frame = BBSCGRectSetY(_lineView1.frame, CGRectGetMaxY(_button1.frame) + 1);
    _button2.frame   = BBSCGRectSetY(_button2.frame, CGRectGetMaxY(_lineView1.frame) + 1);
    _lineView2.frame = BBSCGRectSetY(_lineView2.frame, CGRectGetMaxY(_button2.frame) + 1);
    _button3.frame   = BBSCGRectSetY(_button3.frame, CGRectGetMaxY(_lineView2.frame) + 1);
    _lineView3.frame = BBSCGRectSetY(_lineView3.frame, CGRectGetMaxY(_button3.frame) + 1);
    _button4.frame   = BBSCGRectSetY(_button4.frame, CGRectGetMaxY(_lineView3.frame) + 1);
}


#pragma mark - Image loading

-(void)startIndicator
{
    [_indicatorActivity startAnimating];
}

-(void)setImageFromArrayWithIndex:(NSInteger)ri
{
    [self performSelectorInBackground:@selector(startIndicator)
                           withObject:nil];
    
    @try
    {
        [_button1 setAlpha:1];
        [_button2 setAlpha:1];
        [_button3 setAlpha:1];
        [_button4 setAlpha:1];
        
        nameOfImage = [arrayOfNames objectAtIndex:ri];
        
        NSURL *imageURL = [NSURL URLWithString:[arrayOfImagesURL objectAtIndex:ri]];
        NSData *dataImage = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:dataImage];
       
        [_imageOfFace setImage:image];
        
        [self relaunchTimers];
    }
    @catch (NSException *exception)
    {
        
        NSLog(@"Image N%i:Error",ri);
    }
    [_indicatorActivity stopAnimating];
}

- (void)relaunchTimers
{
    [timer invalidate];
    [spinTimer invalidate];
    
    progress = 0;
    countDown = 10;
    
    [_spinnerView setProgress:0];
    [_coundDownLable setText:@"10"];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(timerIteration)
                                           userInfo:nil
                                            repeats:YES];
    
    spinTimer = [NSTimer scheduledTimerWithTimeInterval:0.01
                                                 target:self
                                               selector:@selector(spinTimerIteration)
                                               userInfo:nil
                                                repeats:YES];
}

- (void)stopTimers
{
    [timer invalidate];
    [spinTimer invalidate];
}

-(void)startGame
{
    [arrayOfImagesURL removeAllObjects];
    [arrayOfNames removeAllObjects];
    
    parser = [[MyParser alloc] init];
    
    [parser letsParseWithURL:urlXML];
    
    [arrayOfNames setArray:[parser arrayOfNames]];
    [arrayOfImagesURL setArray:[parser arrayOfURL]];
    
    arraySize = [arrayOfImagesURL count];
    
    [_buttonNextImage setAlpha:0.9];
    [_buttonGiveMeMoreTime setAlpha:0.9];
    [_button50 setAlpha:0.9];
    
    score = 0;

    [_scoreLabel setText:[NSString stringWithFormat:NSLocalizedString(@"SCORE", nil), score]];

    srandom((unsigned)time(NULL));
    randomIndex = (int)(random() % arraySize);
    
    [self setNamesOnButtonsWithCorrectIndex:randomIndex];
    [self setImageFromArrayWithIndex:randomIndex];
}

#pragma mark - Timer methods

-(void)timerIteration
{
    countDown--;
    
    [_coundDownLable setText:[NSString stringWithFormat:@"%i", countDown]];
   
    alertTimeIsOut = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MSG_TIME_IS_OUT", nil)
                                                message:[NSString stringWithFormat:NSLocalizedString(@"MSG_SCORE_IS", nil),score]
                                               delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"MSG_EXIT", nil)
                                      otherButtonTitles:NSLocalizedString(@"MSG_PLAY_AGAIN", nil), nil];
    
    if (countDown==0)
    {
        [self stopTimers];
        
        [megaPlayer playWhenAnswerIsIncorrect];
        [alertTimeIsOut show];
    }
}

- (void)spinTimerIteration
{
    progress += 0.001;
    [[self spinnerView] setProgress:progress animated:NO];
}

#pragma mark Set Names on buttons

-(void)setNamesOnButtonsWithCorrectIndex:(NSInteger)correctIndex
{
    NSInteger randIndex1, randIndex2, randIndex3, randIndex4;
    
    randIndex1 = correctIndex;
    randIndex2 = correctIndex;
    randIndex3 = correctIndex;
    randIndex4 = correctIndex;
    
    NSMutableArray *aasd = [[NSMutableArray alloc] init];
    
    for (int i=0; i < arraySize; i++)
    {
        [aasd addObject:[NSNumber numberWithInt:i]];
    }
    
    
    while (randIndex1==correctIndex)
    {
        randIndex1 = (int)(random() % arraySize);
    }
    while ((randIndex2==correctIndex)||(randIndex2==randIndex1))
    {
        randIndex2 = (int)(random() % arraySize);
    }
    while ((randIndex3==correctIndex)||(randIndex3==randIndex1)||(randIndex3==randIndex2))
    {
        randIndex3 = (int)(random() % arraySize);
    }
    while ((randIndex4==correctIndex)||(randIndex4==randIndex1)||(randIndex4==randIndex2)||(randIndex4==randIndex3))
    {
        randIndex4 = (int)(random() % arraySize);
    }
    
    correctAnswer = (int)(random() % 4);
    NSMutableArray *randomIndexes = [[NSMutableArray alloc]initWithObjects:[NSNumber numberWithInt:randIndex1], [NSNumber numberWithInt:randIndex2], [NSNumber numberWithInt:randIndex3],[NSNumber numberWithInt:randIndex4], nil];
    [randomIndexes replaceObjectAtIndex:correctAnswer withObject:[NSNumber numberWithInt:correctIndex]];
    correctAnswer++;
    
    [_button1 setTitle:[arrayOfNames objectAtIndex:[[randomIndexes objectAtIndex:0] intValue]] forState:0];
    [_button2 setTitle:[arrayOfNames objectAtIndex:[[randomIndexes objectAtIndex:1] intValue]] forState:0];
    [_button3 setTitle:[arrayOfNames objectAtIndex:[[randomIndexes objectAtIndex:2] intValue]] forState:0];
    [_button4 setTitle:[arrayOfNames objectAtIndex:[[randomIndexes objectAtIndex:3] intValue]] forState:0];
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)whenAnswerIsCorrect
{
    
    score ++;
    [_scoreLabel setText:[NSString stringWithFormat:NSLocalizedString(@"SCORE", nil),score]];
    
    [arrayOfImagesURL removeObjectAtIndex:randomIndex];
    [arrayOfNames removeObjectAtIndex:randomIndex];
    
    arraySize = [arrayOfImagesURL count];
    
    if ([arrayOfImagesURL count]==4)
    {
        [self stopTimers];
        [megaPlayer playWhenWin];
        
        alertYouWon = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MSG_CONGRATULATIONS", nil)
                                                 message:[NSString stringWithFormat:NSLocalizedString(@"MSG_SCORE_IS", nil),score]
                                                delegate:self cancelButtonTitle:NSLocalizedString(@"MSG_EXIT", nil)
                                       otherButtonTitles:NSLocalizedString(@"MSG_PLAY_AGAIN", nil), nil];
        [alertYouWon show];
    }
    else
    {
        [megaPlayer playWhenAnswerIsCorrect];
        randomIndex = (int)(random() % arraySize);
        [self setNamesOnButtonsWithCorrectIndex:randomIndex];
        [self setImageFromArrayWithIndex:randomIndex];
        
        [self relaunchTimers];
    }
}

-(void)whenAnswerIsIncorrect
{
    [megaPlayer playWhenAnswerIsIncorrect];
    [self stopTimers];
    
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MSG_GAME_OVER", nil)
                                       message:[NSString stringWithFormat:NSLocalizedString(@"MSG_SCORE_IS", nil),score]
                                      delegate:self cancelButtonTitle:NSLocalizedString(@"MSG_EXIT", nil)
                             otherButtonTitles:NSLocalizedString(@"MSG_PLAY_AGAIN", nil), nil];
    [alert show];
    score = 0;
}

- (IBAction)pressedButton1:(id)sender
{
    
    [self stopTimers];
    
    if ([sender titleForState:0] == [arrayOfNames objectAtIndex:randomIndex])
    {
        [megaPlayer playWhenAnswerIsCorrect];
        [self whenAnswerIsCorrect];
    }
    else
    {
        [self whenAnswerIsIncorrect];
    }
}


#pragma mark tips

- (IBAction)pressedButtonNextImage:(id)sender
{
    [megaPlayer playWhenTip];
    
    randomIndex = (int)(random() % arraySize);
    
    [self setNamesOnButtonsWithCorrectIndex:randomIndex];
    [self setImageFromArrayWithIndex:randomIndex];
    
    [self relaunchTimers];
    
    [_buttonNextImage setAlpha:0];
}

- (IBAction)pressedButton50:(id)sender
{
    [megaPlayer playWhenTip];
    
    srandom((unsigned)time(NULL));    
    NSInteger rand50;
    do
    {
        rand50 = ((int)(random() % 4) +1);
    }
    while (rand50==correctAnswer);
    
    NSNumber* a = [[NSNumber alloc] initWithFloat:0.0];
    NSMutableArray *array50 = [[NSMutableArray alloc] initWithObjects: a, a, a, a, nil];
    
    [array50 replaceObjectAtIndex:(correctAnswer - 1) withObject:[NSNumber numberWithFloat:1.0]];
    [array50 replaceObjectAtIndex:(rand50 - 1) withObject:[NSNumber numberWithFloat:1.0]];
    
    [_button1 setAlpha:[[array50 objectAtIndex:0] floatValue]];
    [_button2 setAlpha:[[array50 objectAtIndex:1] floatValue]];
    [_button3 setAlpha:[[array50 objectAtIndex:2] floatValue]];
    [_button4 setAlpha:[[array50 objectAtIndex:3] floatValue]];
         
    [_button50 setAlpha:0];
}

- (IBAction)pressedButtonGiveMeMoreTime:(id)sender
{
    [megaPlayer playWhenTip];
    
    [self relaunchTimers];
    
    [_buttonGiveMeMoreTime setAlpha:0];
}

- (IBAction)exitButton:(id)sender
{
    [megaPlayer playWhenExit];
    
    [self stopTimers];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [megaPlayer playWhenLaunch];
        [self startGame];
    }
    else
    {
        [megaPlayer playWhenExit];
        [self performSegueWithIdentifier:@"segue1"
                                  sender:self];
    }
}

#pragma mark - iAD

-(void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
    self.iAdBanner.alpha = 1;
    _lineViewBottom.frame = BBSCGRectSetY(_lineViewBottom.frame, CGRectGetMinY(self.iAdBanner.frame) - 1);
    [UIView commitAnimations];

    [self viewDidLayoutSubviews];
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{    
    return YES;
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
    self.iAdBanner.alpha = 0;
    _lineViewBottom.frame = BBSCGRectSetY(_lineViewBottom.frame, CGRectGetMaxY(self.iAdBanner.frame));
    [UIView commitAnimations];
    
    [self viewDidLayoutSubviews];
}

@end

//
//  ViewController.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 10.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APBaseViewController.h"
#import "HAIMenuViewController.h"
#import "MyParser.h"
#import "MyPlayer.h"
#import "STKSpinnerView.h"
#import "BBSCGRectUtilities.h"

@interface HAIGameViewController : APBaseViewController <NSXMLParserDelegate, UIAlertViewDelegate, ADBannerViewDelegate>
{
    
    NSTimer *timer;
    NSTimer *spinTimer;
    
    NSInteger countDown;
    NSInteger score;
    NSInteger chosenAnswer; //1 - 4
    NSInteger correctAnswer; // 1 -4
    NSString *nameOfImage;
    NSInteger arraySize;

    NSInteger randomIndex;
    
    NSMutableArray *arrayOfImagesURL;
    NSMutableArray *arrayOfNamesRussian;
    NSMutableArray *arrayOfNames;
    NSString *lang;
    
    UIAlertView *alert;
    UIAlertView *alertYouWon;
    UIAlertView *alertTimeIsOut;
    
    NSString *urlXML;
    
    MyParser *parser;
}

@property NSInteger category;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *coundDownLable;
@property (weak, nonatomic) IBOutlet UIImageView *imageOfFace;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (weak, nonatomic) IBOutlet UIButton *buttonNextImage;
@property (weak, nonatomic) IBOutlet UIButton *button50;
@property (weak, nonatomic) IBOutlet UIButton *buttonGiveMeMoreTime;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorActivity;
@property (nonatomic) STKSpinnerView *spinnerView;

//Lines for ui
@property (weak, nonatomic) IBOutlet UIView *lineView1;
@property (weak, nonatomic) IBOutlet UIView *lineView2;
@property (weak, nonatomic) IBOutlet UIView *lineView3;
@property (weak, nonatomic) IBOutlet UIView *lineViewTop;
@property (weak, nonatomic) IBOutlet UIView *lineViewBottom;


- (IBAction)pressedButton1:(id)sender;
- (IBAction)pressedButtonNextImage:(id)sender;
- (IBAction)pressedButton50:(id)sender;
- (IBAction)pressedButtonGiveMeMoreTime:(id)sender;

- (IBAction)exitButton:(id)sender;

@end

//
//  HAICollectionViewCell.h
//  HeroesAndIcons
//
//  Created by Алексей Поляков on 28.10.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HAICollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (strong, nonatomic) IBOutlet UILabel *categoryNameLabel;

- (void)blurOn;

@end

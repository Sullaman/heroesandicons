//
//  HAICollectionViewCell.m
//  HeroesAndIcons
//
//  Created by Алексей Поляков on 28.10.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "HAICollectionViewCell.h"
#import "AMBlurView.h"

@implementation HAICollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)blurOn
{
    CALayer *cellLayer = [self layer];
    cellLayer.cornerRadius = 5;
    cellLayer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.layer.cornerRadius] CGPath];
    
    AMBlurView *blurView = [[AMBlurView alloc] initWithFrame:_categoryNameLabel.frame];
    [self addSubview:blurView];
    [_categoryNameLabel removeFromSuperview];
    [self addSubview:_categoryNameLabel];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

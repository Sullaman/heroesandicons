//
//  MyParser.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 17.03.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "MyParser.h"

@implementation MyParser

- (void)letsParseWithURL:(NSString *)url
{
    _arrayOfURL = [[NSMutableArray alloc] init];
    _arrayOfNames = [[NSMutableArray alloc] init];
    _theParser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    [_theParser setDelegate:self];
    [_theParser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"foo"])
    {
        [_arrayOfURL addObject:[attributeDict objectForKey:@"url"]];
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"ru"])
        {
            [_arrayOfNames addObject:[attributeDict objectForKey:@"rusname"]];
        }
        else
        {
            [_arrayOfNames addObject:[attributeDict objectForKey:@"engname"]];
        }
    }
}

@end

//
//  MyPlayer.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 24.04.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>    

@interface MyPlayer : NSObject
{    
    AVAudioPlayer *thePlayer;
    NSArray *files;
}

-(void)playContentOfFile:(NSString *)name;

-(void)playWhenAnswerIsCorrect;
-(void)playWhenAnswerIsIncorrect;
-(void)playWhenTip;
-(void)playWhenWin;
-(void)playWhenExit;
-(void)playWhenLaunch;
-(void)playWhenLoad;

@end

#import <CoreGraphics/CoreGraphics.h>

CGRect BBSCGRectWithSize(CGFloat width, CGFloat height);
CGRect BBSCGRectWithOrigin(CGFloat x, CGFloat y);

CGRect BBSCGRectSetX(CGRect rect, CGFloat x);
CGRect BBSCGRectSetY(CGRect rect, CGFloat y);

CGRect BBSCGRectSetWidth(CGRect rect, CGFloat width);
CGRect BBSCGRectSetHeight(CGRect rect, CGFloat height);

CGRect BBSCGRectSetXY(CGRect rect, CGFloat x, CGFloat y);
CGRect BBSCGRectSetWidthHeight(CGRect rect, CGFloat width, CGFloat height);
CGRect BBSCGRectSetSize(CGRect rect, CGSize size);

CGRect BBSCGRectCenterLeft(CGRect rect, CGRect ofRect, CGFloat padding);
CGRect BBSCGRectCenterRight(CGRect rect, CGRect ofRect, CGFloat padding);
CGRect BBSCGRectCenterBottom(CGRect rect, CGRect ofRect, CGFloat padding);
CGRect BBSCGRectCenterTop(CGRect rect, CGRect ofRect, CGFloat padding);

CGSize BBSCGSizeAspectScaleToSize(CGSize fromSize, CGSize toSize);

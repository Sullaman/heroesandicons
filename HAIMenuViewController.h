//
//  ViewController2.h
//  Face_Chooser
//
//  Created by Алексей Поляков on 14.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APBaseViewController.h"
#import "HAIGameViewController.h"
#import "MyParser.h"
#import "MyPlayer.h"
#import <iAd/iAd.h>

@interface HAIMenuViewController : APBaseViewController <UICollectionViewDelegate, UICollectionViewDataSource, ADBannerViewDelegate>
{
    NSMutableArray *arrayOfCategoryes;
    NSMutableArray *arrayOfCategorysNames;
    MyParser *categoryParser;
    MyPlayer *player;
}

@property (nonatomic) NSMutableDictionary *allInformation;
@property (nonatomic,assign) BOOL bannerIsVisible;

@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UICollectionView *categoryCollectionView;

+ (NSString *)categoryURL;

@end

//
//  ViewController2.m
//  Face_Chooser
//
//  Created by Алексей Поляков on 14.02.13.
//  Copyright (c) 2013 Алексей Поляков. All rights reserved.
//

#import "HAIMenuViewController.h"
#import "HAICollectionViewCell.h"
#import "AMBlurView.h"

NSString *kPlistFileURL = @"http://podcast.gamebolt.ru/app/ios/hai.plist";
NSString *kCategoriesKey = @"categoryes";
NSString *kSegueIdentifier = @"toGameView";

@implementation HAIMenuViewController

static NSString *categoryURL;
static NSInteger y;
static NSInteger yForBigScr;
static NSInteger i;

- (void)viewDidLoad
{
    [super viewDidLoad];

    _allInformation = [NSMutableDictionary dictionaryWithContentsOfURL:[NSURL URLWithString:kPlistFileURL]];
    
    _categoryCollectionView.delegate = self;
    _categoryCollectionView.dataSource = self;
    _categoryCollectionView.alpha = 0;
    _categoryCollectionView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.iAdBanner];
    self.iAdBanner.delegate = self;
    self.iAdBanner.backgroundColor = [UIColor clearColor];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *v = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:v];
    [v setImage:[UIImage imageNamed:@"launch-568.png"]];
    [self.view sendSubviewToBack:v];
    AMBlurView *blurView = [[AMBlurView alloc] initWithFrame:self.view.bounds];
    [self.view insertSubview:blurView belowSubview:_logoImage];
    
    if (self.iAdBanner.isBannerLoaded)
    {
        self.iAdBanner.alpha = 1;
    }
    else
    {
        self.iAdBanner.alpha = 0;
    }

    arrayOfCategorysNames = [[NSMutableArray alloc] init];
    arrayOfCategoryes = [[NSMutableArray alloc] init];
    
    categoryParser = [[MyParser alloc] init];
    [categoryParser letsParseWithURL:@"http://podcast.gamebolt.ru/index.xml"];
   
    [arrayOfCategorysNames setArray:[categoryParser arrayOfNames]];
    [arrayOfCategoryes setArray:[categoryParser arrayOfURL]];
    
    if (i != 21)
    {
        player = [[MyPlayer alloc] init];
        [player playWhenLoad];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_logoImage setAlpha:1];
    
    y = 193;
    yForBigScr = 223;
    
    if (i != 21)
    {
        CGRect frame = self.logoImage.frame;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        if (screenRect.size.height == 568.0f)
        {
            frame.origin.y = (float)yForBigScr;
        }
        else frame.origin.y = y;
        
        self.logoImage.frame = frame;
    
        [UIView animateWithDuration:1.0 delay:0.2 options:0
                         animations:^{
             CGRect frame = self.logoImage.frame;
             frame.origin.y = 40.0;
             self.logoImage.frame = frame;
         }
                     completion:^(BOOL completed)
         {
             [_categoryCollectionView setAlpha:1];
             i = 21;
         }];
    }
    else
    {
        [_categoryCollectionView setAlpha:1];
    }
}
  
+ (NSString *)categoryURL
{
    return categoryURL;
}

#pragma mark CollectionView protocol implementaion

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[_allInformation objectForKey:kCategoriesKey] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HAICollectionViewCell *theCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"categoryCell"
                                                                               forIndexPath:indexPath];
    
    if (!theCell)
    {
        theCell = [[HAICollectionViewCell alloc] init];
    }
    
    NSDictionary *currentCategory = [[_allInformation objectForKey:kCategoriesKey] objectAtIndex:indexPath.row];
    NSInteger heroesCount = [[[currentCategory objectForKey:@"heroes"] objectForKey:@"male"] count];
    srandom((unsigned)time(NULL));
    NSInteger randomIndex = (int)(random() % heroesCount);
    
    UIImage *tmpImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[[currentCategory objectForKey:@"heroes"] objectForKey:@"male"] objectAtIndex:randomIndex] objectForKey:@"image"]]]];
    
    theCell.categoryImageView.image = tmpImage;
    theCell.categoryNameLabel.textColor = [UIColor blackColor];
    theCell.categoryNameLabel.text = [currentCategory objectForKey:@"categoryNameRUS"];
    
    [theCell blurOn];
    
    return theCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    categoryURL = [[categoryParser arrayOfURL] objectAtIndex:indexPath.row];
    
    i = 21;
    
    [self performSegueWithIdentifier:kSegueIdentifier
                              sender:self];
}

#pragma mark - iAD

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
    self.iAdBanner.alpha = 1;
    [UIView commitAnimations];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
    self.iAdBanner.alpha = 0;
    [UIView commitAnimations];
}


@end
